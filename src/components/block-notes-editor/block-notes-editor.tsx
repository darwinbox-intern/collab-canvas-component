import "@blocknote/core/fonts/inter.css";
import "@blocknote/react/style.css";
// import "@blocknote/mantine/style.css";
import {handleTableInsertion} from '../../utils/tableUtils/tableInsertion';
import { AgTable } from "../ag-table/ag-table";
import { RiAlertFill, RiTable2, RiBracesLine } from "@remixicon/react";
import React, { useRef } from 'react';
import {SuggestionMenuController, BlockNoteView,getDefaultReactSlashMenuItems, useCreateBlockNote } from "@blocknote/react";
import { insertOrUpdateBlock, defaultBlockSpecs, BlockNoteSchema, filterSuggestionItems,} from "@blocknote/core";
import { WebsocketProvider } from "y-websocket";
import { defaultInlineContentSpecs, defaultStyleSpecs } from "@blocknote/core";
import {DefaultReactSuggestionItem} from "@blocknote/react";
import {Mention} from '../inline/mention/Mention';
import * as Y from 'yjs';

const schema = BlockNoteSchema.create({
	blockSpecs: {
		...defaultBlockSpecs,
		agTable: AgTable,
	},
	inlineContentSpecs: {
		...defaultInlineContentSpecs,
		mention: Mention,
	},
});

export function BlockNotesEditor(props: any) {
	console.log('Editor props', props)
	const isEditable = props.isEditable;
	const docId = props.document.id;
	const darkThemeMq = window.matchMedia("(prefers-color-scheme: dark)");
	console.log("Dark theme", darkThemeMq.matches);

	const users = [{username: 'ujwal'}, {username: 'uday'}]

	console.log('DOc id', docId);
	const ydoc = new Y.Doc();
	const socketProvider = new WebsocketProvider(
		"ws://localhost:5000",
		docId ? docId : "quill-demo-room",
		ydoc
	);
	const inputExcelRef = useRef<HTMLInputElement>(null);
	const inputLinkRef = useRef<HTMLInputElement>(null);
	const inputJsonRef = useRef<HTMLInputElement>(null);
	const buttonBlankRef = useRef<HTMLInputElement>(null);
	const closeButtonRef = useRef<HTMLInputElement>(null);

	const editor = useCreateBlockNote({
		schema,
		collaboration: {
			provider: socketProvider,
			fragment: ydoc.getXmlFragment('document-store'),
			user: {
				name: props.user.name || "user",
				color: "#ff0000",
			},
		},
	});

	// const insertGoalTable = (editor: any) => ({
	// 	title: "Goal Table",
	// 	onItemClick: async() => {
	// 		insertOrUpdateBlock(editor, {
	// 			type: "agTable",
	// 		});
	// 		const url = window.prompt("Enter source");
	// 		if (url) {
	// 			const response = await fetch(url);

	// 			if (response.ok) {
	// 				const json = await response.json();
	// 				console.log("Json fetch data", json);
					
	// 				// editor.updateBlock(block, {
	// 				// 	type: "agTable",
	// 				// 	props: {
	// 				// 		content: JSON.stringify(json),
	// 				// 	},
	// 				// });
	// 				// insertOrUpdateBlock(block, {
	// 				// 	type: "agTable",
	// 				// 	props: {
	// 				// 		content: JSON.stringify(json),
	// 				// 	},
	// 				// });
	// 				insertOrUpdateBlock(editor, {
	// 					type: "agTable",
	// 					props: {
	// 						content: JSON.stringify([{name: 'Ujwal', age: 18}]) as never,
	// 					},
	// 				});
	// 			}
	// 		} 			
	// 	},
	// 	aliases: ["table"],
	// 	group: "Table",
	// 	icon: <RiTable2 />,
	// });

    // const insertAPIAGTable = (editor:any) => ({
	// 		title: "API Table(Advanced)",
	// 		onItemClick: async() => {
	// 			console.log("alert clicked");				
    //             const url = window.prompt('Enter source');
	// 			if (url) {
	// 				const response = await fetch(url);
	// 				if (response.ok) {
	// 					const json = await response.json();
	// 					console.log("Json fetch data", json);
	// 					insertOrUpdateBlock(editor, {
	// 						type: "agTable",
	// 						props: {
	// 							content: JSON.stringify(json) as never,
	// 						},
	// 					});
	// 				}
	// 			} 
	// 		},
	// 		aliases: ["table"],
	// 		group: "Table",
	// 		icon: <RiAlertFill />,
	// });
	
	const insertExcelTable = (editor: any) => ({
		title: "Excel Table",
		onItemClick: () => {
			console.log("alert clicked");
			insertOrUpdateBlock(editor, {
				type: "table",
			});
		},
		aliases: ["table"],
		group: "Table",
		icon: <RiTable2 />,
	});

	const insertExcelAGTable = (editor: any) => ({
		title: "Excel Table(Advanced)",
		onItemClick: () => {
			console.log("alert clicked");
			insertOrUpdateBlock(editor, {
				type: "agTable",
			});
		},
		aliases: ["table"],
		group: "Table",
		icon: <RiTable2 />,
	});

	const insertJsonTable = (editor: any) => ({
		title: "JSON Table",
		onItemClick: () => {
			console.log("alert clicked");
			insertOrUpdateBlock(editor, {
				type: "table",
			});
		},
		aliases: ["table"],
		group: "Table",
		icon: <RiBracesLine />,
	});

	const insertJsonAGTable = (editor: any) => ({
		title: "JSON Table(Advanced)",
		onItemClick: () => {
			insertOrUpdateBlock(editor, {
				type: "agTable",
			});
		},
		aliases: ["table"],
		group: "Table",
		icon: <RiBracesLine />,
	});

	const insertAPITable = (editor: any) => ({
		title: "API Table",
		onItemClick: () => {
			console.log("alert clicked");
			insertOrUpdateBlock(editor, {
				type: "table",
			});
		},
		aliases: ["table"],
		group: "Table",
		icon: <RiTable2 />,
	});

	const insertAPIAGTable = (editor: any) => ({
		title: "API Table(Advanced)",
		onItemClick: () => {
			console.log("alert clicked");
			insertOrUpdateBlock(editor, {
				type: "agTable",
			});
		},
		aliases: ["table"],
		group: "Table",
		icon: <RiTable2 />,
	});

	const insertGoalTable = (editor: any) => ({
		title: "Goal Table",
		onItemClick: () => {
			insertOrUpdateBlock(editor, {
				type: "agTable",
			});
		},
		aliases: ["table"],
		group: "Table",
		icon: <RiTable2 />,
	});

	function addNotificationToUser(): any {

	}

	const getMentionMenuItems = (
		editor: typeof schema.BlockNoteEditor,
		users: any[] | undefined,
		currentUser: string | undefined | null,
		addNotificationToUser: (
			mentionedUserId: string,
			mentionedBy: string,
			mentionId: string
		) => Promise<void>
	): DefaultReactSuggestionItem[] => {
		const userss = ["Steve", "Bob", "Joe", "Mike"];
		return userss.map((user) => ({			
			title: user,
			onItemClick: () => {
				editor.insertInlineContent([
					{
						type: "mention",
						props: {
							user,
						} as any,
					},
					" ",
				]);
			},
		}));

		if (!users) {
			return [];
		}
		if (!currentUser) {
			return [];
		}
		const userNames: string[] = users
			? users
			: []
					.filter((user: any) => user && user.name) // Filter out undefined or null values
					.map((user: any) => user!.name);

		return userNames?.map((user, idx) => {
			return {
				title: user,
				onItemClick: () => {
					const mentionId = `mention-${users[idx].userId}-${Date.now()}`;
					const userId: string = String(users[idx].userId);
					addNotificationToUser(userId, currentUser, mentionId);
					editor.insertInlineContent([
						{
							type: "mention",
							props: {
								user,
								mentionId: mentionId,
							},
						},
						" ",
					]);
				},
			};
		});
	};

	return (
		<>
			<input type="file" hidden ref={inputExcelRef} id="excel" />
			<input type="file" hidden ref={inputJsonRef} id="json" />
			<input type="text" hidden ref={inputLinkRef} id="link" />
			<input type="button" hidden ref={buttonBlankRef} id="button" />
			<input type="button" hidden ref={closeButtonRef} id="close" />
			<BlockNoteView
				editable={isEditable}
				editor={editor}
				slashMenu={false}				
				theme={darkThemeMq.matches ? "dark" : "light"}
			>
				<SuggestionMenuController
					triggerCharacter={"/"}
					getItems={async (query) =>
						filterSuggestionItems(
							[
								...getDefaultReactSlashMenuItems(editor),
								insertAPITable(editor),
								insertAPIAGTable(editor),
								insertGoalTable(editor),
								insertExcelAGTable(editor),
								insertExcelTable(editor),
								insertJsonAGTable(editor),
								insertJsonTable(editor)
							],
							query
						)
					}
					onItemClick={async (e) =>
						await handleTableInsertion(
							e,
							buttonBlankRef,
							inputExcelRef,
							inputLinkRef,
							inputJsonRef,
							closeButtonRef,
							insertOrUpdateBlock,
							editor
						)
					}
				/>

				<SuggestionMenuController
					triggerCharacter={"@"}
					getItems={async (query) =>
						filterSuggestionItems(
							getMentionMenuItems(
								editor,
								[{ title: "ujwal" }],
								"uday",
								addNotificationToUser
							),
							query
						)
					}
				/>
			</BlockNoteView>
		</>
	);
}

