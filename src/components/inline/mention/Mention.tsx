import { createReactInlineContentSpec } from "@blocknote/react";
import React from "react";
 
export const Mention = createReactInlineContentSpec(
  {
    type: "mention",
    propSchema: {
      user: {
        default: "Unknown",
      },
      mentionId:{
        default:""
      },
      
    },
    content: "none",
  },
  {
    render: (props) => {
      const anchorId = `mention-anchor-${props.inlineContent.props.mentionId}`;

      React.useEffect(() => {
        const anchorElement = document.createElement('span');
        anchorElement.id = anchorId;
        anchorElement.style.display = 'inline-block';
        anchorElement.style.height = '0';
        anchorElement.style.visibility = 'hidden';

        const mentionSpan = document.getElementById(`mention-span-${props.inlineContent.props.mentionId}`);
        if (mentionSpan) {
          mentionSpan.parentNode?.insertBefore(anchorElement, mentionSpan);
        }

        return () => {
          const anchorToRemove = document.getElementById(anchorId);
          if (anchorToRemove) {
            anchorToRemove.remove();
          }
        };
      }, [anchorId]);

      return (
        <span id={`mention-span-${props.inlineContent.props.mentionId}`} style={{ backgroundColor: '#8400ff33' }}>
          @{props.inlineContent.props.user}          
        </span>
      );
    },
  }
);
 