import React, { useEffect, useState } from 'react'
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-quartz.css";

const AgTableUtil = ({props}:any) => {
			console.log("UTILLLLL props", props)
			useEffect(() => {
				async function fetchData() {
					const url = props.block.props.url;
					console.log("URL", url);
					const response = await fetch(url, {
						method: "POST",
					});
					console.log("Response", response);
					if (response.ok) {
						const json = await response.json();
						console.log("JSON data", json);
						console.log("Json fetch data", json);

						props.editor.updateBlock(props.block, {
							type: "agTable",
							props: {
								content: JSON.stringify(json),
							},
						});
					}
				}
				console.log("PROPSSSS", props);
				if (props.block.props.type === "goal") {
					fetchData();
				}
			}, []);

			const [newColumn, setNewColumn] = useState("");
			const data = JSON.parse(props.block.props.content);
			console.log('DATAAA', data)

			const headersSet = new Set();
			data.forEach((obj:any) => {
				Object.keys(obj).forEach((field) => {
					headersSet.add(field);
				});
			});
			const headers = Array.from(headersSet);
			console.log('Headers', headers)

			// const { resolvedTheme } = useTheme();

			// console.log("THEME", resolvedTheme);
			const columns = headers.map((ele) => {
				return {
					field: ele,
					editable: props.editor.isEditable,
					filter: true,
				};
			});
			const rowsData = data;

			function handleAddData() {
				console.log("Rowsdata", rowsData);
				rowsData.push(data);

				props.editor.updateBlock(props.block, {
					type: "agTable",
					props: {
						content: JSON.stringify(rowsData),
					},
				});
			}

			function handleAddRow() {
				const row:any = {};
				headers.forEach(function (col:any) {
					row[col] = "-";
				});

				rowsData.push(row);
				props.editor.updateBlock(props.block, {
					type: "agTable",
					props: {
						content: JSON.stringify(rowsData),
					},
				});
			}

			// function handleAddNewColumn() {
			// 	if (newColumn === "" || newColumn in rowsData[0]) {
			// 		setNewColumn("");
			// 		return;
			// 	}

			// 	rowsData[0][newColumn] = "";
			// 	setNewColumn("");

			// 	props.editor.updateBlock(props.block, {
			// 		type: "agTable",
			// 		props: {
			// 			content: JSON.stringify(rowsData),
			// 		},
			// 	});
			// }

			return (
				<div ref={props.contentRef} className="main w-full">
					<div className="flex gap-2 w-full items-start">
						<div
							style={{
								height: Math.min(rowsData.length * 100, 500),
								width: "90%",
							}}
							className={
									"ag-theme-quartz"
							}
						>
							<AgGridReact
								columnDefs={columns as any}
								rowData={rowsData}
								pagination={true}
								paginationPageSize={10}
								paginationPageSizeSelector={[20, 50, 100]}
								onCellValueChanged={(det) => {
									console.log("Det", det);
									props.editor.updateBlock(props.block, {
										type: "agTable",
										props: {
											content: JSON.stringify(rowsData),
										},
									});
									console.log("Rows data", rowsData);
								}}
							/>
						</div>
					</div>
					<button
						className="border w-3/4 mt-2 p-1  flex items-center border-black justify-center border rounded-md hover:bg-slate-200"
						onClick={handleAddRow}
						hidden={!props.editor.isEditable}
						style={{
							width: "90%",
						}}
					>
						+
					</button>
				</div>
			);
}

export default AgTableUtil