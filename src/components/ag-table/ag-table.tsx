import React, { useEffect, useState } from "react";
import { RiAlertFill } from "@remixicon/react";
import { createReactBlockSpec } from "@blocknote/react";
import { defaultProps, insertOrUpdateBlock } from "@blocknote/core";
import "ag-grid-community/styles/ag-grid.css";
import AgTableUtil from "./ag-table.util";
import "ag-grid-community/styles/ag-theme-quartz.css";
import { AgGridReact } from "ag-grid-react";

export const insertAgTable = (editor:any) => ({
	title: "AG Table",
	onItemClick: () => {
		console.log("AG clicked");
		insertOrUpdateBlock(editor, {
			type: "agTable",			
		});
	},
	content: "table",
	aliases: [
		"alert",
		"notification",
		"emphasize",
		"warning",
		"error",
		"info",
		"success",
	],
	group: "custom",
	icon: <RiAlertFill />,
});

export const AgTable = createReactBlockSpec(
	{
		type: "agTable",
		propSchema: {
			textAlignment: defaultProps.textAlignment,
			textColor: defaultProps.textColor,
			content: {
				default: JSON.stringify([]),
			},
			type: {
				default: "",
			},
			url: {
				default: "",
			},
		},
		content: "none",
	},
	{
		render: (props) => {			
			useEffect(() => {
				async function fetchData() {
					const url = props.block.props.url;
					const response = await fetch(url, {
						method: "POST",
					});
					console.log("Response", response);
					if (response.ok) {
						const json = await response.json();
						console.log("JSON data", json);
						console.log("Json fetch data", json);
						console.log("PROPPPPPPPPPSSSSSSSS AGGGGG", props);
						props.editor.updateBlock(props.block, {
							type: "agTable",
							props: {
								content: JSON.stringify(json),
							},
						});
					}
				}
				console.log("PROPSSSS", props);
				if (
					props.block.props.type === "goal"
				) {
					fetchData();
				}
			}, []);

			const [newColumn, setNewColumn] = useState("");
			const data = JSON.parse(props.block.props.content);

			const headersSet: Set<string> = new Set();
			data.forEach((obj: any) => {
				Object.keys(obj).forEach((field: string) => {
					headersSet.add(field);
				});
			});
			const headers: string[] = Array.from(headersSet);


			const columns: any = headers.map((ele) => {
				if(props.block.props.type === "goal" && ele == '_id') {
					return {
						field: ele,
						editable: props.editor.isEditable,
						filter: true,
						hide: true
					}
				}
				return {
					field: ele,
					editable: props.editor.isEditable,
					filter: true,
				};
			});
			const rowsData = data;

			function handleAddData() {
				console.log("Rowsdata", rowsData);
				rowsData.push(data);

				props.editor.updateBlock(props.block, {
					type: "agTable",
					props: {
						content: JSON.stringify(rowsData),
					},
				});
			}

			function handleAddRow() {
				const row: any = {};
				headers.forEach(function (col: string) {
					row[col] = "-";
				});

				rowsData.push(row);
				props.editor.updateBlock(props.block, {
					type: "agTable",
					props: {
						content: JSON.stringify(rowsData),
					},
				});
			}

			function handleAddNewColumn() {
				const newColumn = window.prompt('Enter new column name');
				if (newColumn === "" || newColumn in rowsData[0]) {
					setNewColumn("");
					return;
				}

				rowsData[0][newColumn] = "";
				setNewColumn("");

				props.editor.updateBlock(props.block, {
					type: "agTable",
					props: {
						content: JSON.stringify(rowsData),
					},
				});
			}

			return (
				<div
					ref={props.contentRef}
					className="main w-full"
					style={{
						display: "flex",
						flexDirection: "column",
						gap: "4px",
					}}
				>
					<div className="gap-2 w-full items-start flex">
						<div
							className="ag-theme-quartz flex"
							style={{
								height: Math.min(rowsData.length * 100, 500),
								width: "90%",
							}}
						>
							<AgGridReact
								columnDefs={columns}
								rowData={rowsData}
								pagination={true}
								paginationPageSize={10}
								paginationPageSizeSelector={[20, 50, 100]}
								onCellValueChanged={async(det) => {
									console.log("Dettt", det);
									props.editor.updateBlock(props.block, {
										type: "agTable",
										props: {
											content: JSON.stringify(rowsData),
										},
									});
									const data = det.data;
									const {_id,...rest}=data;
									const bodyData = JSON.stringify({
										id:_id.$oid,
										content: rest
									})

									const url = "http://localhost/DarwinDocs/Tasks/updateTask"
									try {
										const response = await fetch(url, {
											method: "POST",
											body: bodyData,
											headers: {
												"Content-Type": "application/json",											
											},
										});
										console.log('Response', response)
								}
								catch(err) {
									console.log('ERrrororo', err)
								}
									console.log("Rows data", rowsData);
									console.log('id changed', det.data._id)
								}}
							/>
						</div>
						{props.block.props.type != "goal" && (
							<button
								className="border mt-2 p-1 flex items-center border-black justify-center border rounded-md hover:bg-slate-200"
								onClick={handleAddNewColumn}
								hidden={!props.editor.isEditable}
								style={{
									width: "90%",
									padding: "5px",
									marginTop: "10px",
									borderRadius: "7px",
									border: "none",
									cursor: "pointer",
								}}
							>
								Insert column
							</button>
						)}
					</div>
					<button
						className="border w-3/4 mt-2 p-1  flex items-center border-black justify-center border rounded-md hover:bg-slate-200"
						onClick={handleAddRow}
						hidden={!props.editor.isEditable}
						style={{
							width: "90%",
							padding: "5px",
							borderRadius: "7px",
							border: "none",
							cursor: "pointer",
						}}
					>
						Insert Row
					</button>
				</div>
			);
		},
	}
);
