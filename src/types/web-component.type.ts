import { ComponentType } from "react"

export type WebComponentRegistration = {
    tag: string,
    reactComponent: ComponentType<any>,
    shadow?: 'open' | 'closed'
}