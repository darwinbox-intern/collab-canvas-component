import { HelloWorld } from "./components/hello-world/hello-world";
import {BlockNotesEditor} from './components/block-notes-editor/block-notes-editor'
import { WebComponentRegistration } from './types/web-component.type'
import { registerWebComponents } from './utils/web-component/web-component.util'

(function () {
    const components: Array<WebComponentRegistration> = [
        { tag: 'dbx-hello-world', reactComponent: HelloWorld}, 
        { tag: 'dbx-block-notes', reactComponent: BlockNotesEditor}
    ];
    registerWebComponents(components);
})();





