import { WebComponentRegistration } from "../../types/web-component.type";
import r2wc from "@r2wc/react-to-web-component"
export function registerWebComponents(reactComponents: Array<WebComponentRegistration>) {
    if(!Array.isArray(reactComponents)) {
        console.warn("[React Web Components] Couldn't Register Web Components.");
        return;
    }
    for(let reactComponent of reactComponents) {
        if(!reactComponent || !reactComponent?.tag || !reactComponent?.reactComponent) {
           continue;
        }
        try {
            registerWebComponent(reactComponent);   
        } catch(E) {
            console.error(`[React Web Components] Error While Declaring Web Component (<${reactComponent}>)`, E);
        }
    }
}

function registerWebComponent(reactComponent: WebComponentRegistration) {
    if(customElements.get(reactComponent?.tag)) {
        console.warn(`[React Web Components] Re-defining <${reactComponent?.tag}>`);
    }
    const customElementDefinition = r2wc(reactComponent.reactComponent,{shadow: reactComponent.shadow , 
        props: {        
            isEditable: "boolean",
            user: "json",
            document: "json",
        }
    });
    customElements.define(reactComponent.tag, customElementDefinition);    
}