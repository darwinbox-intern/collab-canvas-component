import * as XLSX from "xlsx";
interface String {
	capitalize(): string;
}

Object.defineProperty(String.prototype, "capitalize", {
	value: function () {
		return this.charAt(0).toUpperCase() + this.slice(1);
	},
	enumerable: false,
});

const jsonData = [
	{
		name: "Ujwal",
		age: 21,
		"blood group": "O+",
	},
	{
		name: "Tes1",
		age: 23,
		"blood group": "O-",
	},
	{
		name: "Mark",
		age: 29,
		"blood group": "A+",
		role: "Tech Lead",
	},
	{
		name: "John",
		role: "SDE",
	},
];

const getData = async (url: string) => {
	// const res = await fetch(url);
	const response = await fetch(url, {
		method: 'GET',
		// headers: {
		//   'Content-Type': 'application/json' // Specify content type as JSON
		// },
		// body: JSON.stringify({}) // Replace {} with your request body data
	  });
	  if (!response.ok) {
		throw new Error(`Failed to fetch data: ${response.statusText}`);
	  }
	//   const jsonData = await response.json();
	const movies = await response.json();
	console.log("Fetched data", movies);
	return movies;
};

export const handleFileUpload = (e: any, selector: string) => {
	console.log("Evenet: ", e);
	const file = e.target.files[0];
	const reader = new FileReader();
	if(selector == 'excel') {
		return new Promise((res, rej) => {
			reader.onload = (event: any) => {
				const binaryString = event.target.result;
				const workbook = XLSX.read(binaryString, { type: "binary" });
				const sheetName = workbook.SheetNames[0];
				const sheet = workbook.Sheets[sheetName];
				const data = XLSX.utils.sheet_to_json(sheet);
				console.log("Data", data);
				res(data);				
			};

			reader.readAsBinaryString(file);
		});
	}
	else if(selector === 'json') {
		return new Promise((res, rej) => {
			console.log('E', e)
			reader.onload = (event: any) => {
				console.log('Event reader', event)
				const jsonData = JSON.parse(event.target.result);
				console.log("JSON DATA", jsonData);
				res(jsonData);				
			}			

			reader.readAsText(file);
		})
	}
};

export const getTableData = async (url: string | null) => {
	if(url == null) {
		return;
	}
	const response = await getData(url);
	console.log("Get res", response);
	let headerObj: any = {};
	const rows: any[] = [{}];

	let counter = 0;
	response.map((data: any) => {
		for (let key in data) {
			if (headerObj[key] === undefined) {
				headerObj[key] = counter;
				counter += 1;
			}
		}
	});

	const row: any = [];
	for (let key in headerObj) {
		const idx = headerObj[key];
		const data: any = new String(key);
		row[idx] = [
			{
				type: "text",
				text: data.capitalize(),
				styles: { bold: true },
			},
		] as never;
	}
	const finalRow = { cells: row };
	rows[0] = finalRow;

	response.map((data: any) => {
		const row: any = [];

		for (let key in data) {
			const idx = headerObj[key];
			row[idx] = [
				{
					type: "text",
					text: new String(data[key]),
					styles: {},
				},
			] as never;
		}
		for (let i = 0; i < counter; i++) {
			if (row[i] === undefined) {
				row[i] = [
					{
						type: "text",
						text: "",
						styles: {},
					},
				] as never;
			}
		}
		const finalRow = { cells: row };
		rows.push(finalRow);
	});

	return rows;
};

export const getTableDataFromJSON = (jsonData: any) => {
	const response = jsonData;
	console.log("Response json", response);
	let headerObj: any = {};
	const rows = [{}];

	let counter = 0;
	response.map((data: any) => {
		for (let key in data) {
			if (headerObj[key] === undefined) {
				headerObj[key] = counter;
				counter += 1;
			}
		}
	});

	const row: any = [];
	for (let key in headerObj) {
		const idx = headerObj[key];
		const data: any = new String(key);
		row[idx] = [
			{
				type: "text",
				text: data.capitalize() === "__EMPTY" ? "" : data.capitalize(),
				styles: { bold: true },
			},
		] as never;
	}
	const finalRow = { cells: row };
	rows[0] = finalRow;

	response.map((data: any) => {
		const row: any = [];

		for (let key in data) {
			const idx = headerObj[key];
			row[idx] = [
				{
					type: "text",
					text: new String(data[key]),
					styles: {},
				},
			] as never;
		}
		for (let i = 0; i < counter; i++) {
			if (row[i] === undefined) {
				row[i] = [
					{
						type: "text",
						text: "",
						styles: {},
					},
				] as never;
			}
		}
		const finalRow = { cells: row };
		rows.push(finalRow);
	});

	return rows;
};

// export const rows = await getTableData('alkdsfj');
