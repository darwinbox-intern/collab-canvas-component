import { getTableDataFromJSON, handleFileUpload, getTableData } from "./getData";

//TODO:
const waitForFile = (ref: any) => {
    console.log('Wait', ref);
    ref.current.click();
	return new Promise((resolve, reject) => {
		// console.log('Here promise')
		ref.current.addEventListener(
			"change",
			(e: React.ChangeEvent<HTMLInputElement>) => {
				const file = e;
				// console.log("Wait for file", file);
				if (file) {
					resolve(file);
				} else {
					reject(new Error("No file selected"));
				}
			}
		);
	});
};

let inputExcelRef: any, inputLinkRef: any, buttonBlankRef: any, closeButtonRef: any, inputJsonRef: any, insertOrUpdateBlock: any, editor: any;

//TODO:
export const removeListeners = () => {
        if(inputExcelRef?.current) {
            inputExcelRef.current.removeEventListener("click", handleExcelClick);
        }
        if(inputLinkRef?.current) {
            console.log('Link remove');
            inputLinkRef.current.removeEventListener("click", handleLinkClick);
        }
        if (buttonBlankRef?.current) {            
			buttonBlankRef.current.removeEventListener("click", handleBlankClick);
		}
        if (closeButtonRef?.current) {
			closeButtonRef.current.removeEventListener("click", removeListeners);
		}
        if(inputJsonRef?.current) {
            inputJsonRef.current.removeEventListener(
							"click",
							handleJSONClick
						);
        }
}

//TODO:
const handleExcelClick = () => {
        waitForFile(inputExcelRef).then((data) => {            
            handleFileUpload(data, 'excel')?.then((jsonData: any) => {
                const rowsData = getTableDataFromJSON(jsonData);
                insertOrUpdateBlock(editor, {
									type: "table",
									content: {
										type: "tableContent",
										rows: rowsData
									},
								}); 
                if(inputExcelRef?.current !== null) {
                    inputExcelRef.current.value = "";
                }                
            })
        });
        removeListeners();
}

const handleExcelAGClick = () => {
        waitForFile(inputExcelRef).then((data) => {            
            handleFileUpload(data, 'excel')?.then((jsonData: any) => {
                // const rowsData = getTableDataFromJSON(jsonData);
                insertOrUpdateBlock(editor, {
									type: "agTable",
									props: {
                                        content: JSON.stringify(jsonData)
                                    }
								}); 
                if(inputExcelRef?.current !== null) {
                    inputExcelRef.current.value = "";
                }                
            })
        });
        removeListeners();
}

//TODO:
const handleBlankClick = () => {
        removeListeners();
        (false);
        insertOrUpdateBlock(editor, {
            type: "table",
            content: {
                type: "tableContent",
                rows: [
                    {
                        cells: ["A1", "A2"],
                    },
                ],
            },
        });     
        removeListeners();
}

//TODO:
const handleLinkClick = async () => {
        console.log('Link click')
        const url: string | null = prompt("Enter the source");        
        // const url : string = 'http://localhost/DarwinDocs/Tasks/getTasks';
        const rowsData = await getTableData(url);
        console.log("Rowss Data", rowsData);
        insertOrUpdateBlock(editor, {
            type: "table",
            content: {
                type: "tableContent",
                rows: rowsData,
            },
        });        
        removeListeners(); 
}

const handleJSONClick = () => {
    waitForFile(inputJsonRef).then((data) => {
        console.log('Wait for file', data);
        handleFileUpload(data, 'json')?.then((jsonData: any) => {
            console.log('Json data', jsonData)
            const rowsData = getTableDataFromJSON(jsonData);
            insertOrUpdateBlock(editor, {
                type: "table",
                content: {
                    type: "tableContent",
                    rows: rowsData,
                },
            });
            if (inputJsonRef?.current !== null) {
                inputJsonRef.current.value = "";
            }
        });
    });
    removeListeners();
}

const handleJSONAGClick = () => {
    waitForFile(inputJsonRef).then((data) => {
        console.log('Wait for file', data);
        handleFileUpload(data, 'json')?.then((jsonData: any) => {
            console.log('Json data', jsonData)
            const rowsData = getTableDataFromJSON(jsonData);
            insertOrUpdateBlock(editor, {
							type: "agTable",
							props: {
								content: JSON.stringify(jsonData),
							},
						}); 
            if (inputJsonRef?.current !== null) {
                inputJsonRef.current.value = "";
            }
        });
    });
    removeListeners();
}

//TODO:
export const handleTableInsertion = async (det: any, buttonBlankReff: any, inputExcelReff: any, inputLinkReff: any, inputJsonReff: any, closeButtonReff: any, insertOrUpdateBlockk: any, editorr: any) => {
        buttonBlankRef = buttonBlankReff;
        inputExcelRef = inputExcelReff;
        inputLinkRef = inputLinkReff;
        inputJsonRef = inputJsonReff;
        closeButtonRef = closeButtonReff;
        insertOrUpdateBlock = insertOrUpdateBlockk;
        editor = editorr;
        

        console.log('Click', det)
            if (det.title === "Table") { 
				// tableOption.onOpen();

                buttonBlankRef?.current?.addEventListener('click', 
                handleBlankClick);

                inputExcelRef?.current?.addEventListener("click", 
                handleExcelClick);
                
                inputLinkRef?.current?.addEventListener("click", 
                handleLinkClick);
                                            
                inputJsonRef?.current?.addEventListener("click", handleJSONClick);

                if(closeButtonRef.current !== null) {
                    closeButtonRef.current.addEventListener('click', removeListeners);
                }
                handleBlankClick();
            }								
            else if (det.title === "Heading 1") {
                insertOrUpdateBlock(editor, {
                    type: "heading",
                    props: {
                        level: 1,
                    },
                });
            } else if (det.title === "Heading 2") {
                const block = insertOrUpdateBlock(editor, {
                    type: "heading",
                    props: {
                        level: 2,
                    },
                });

                console.log('BLOCKKKK', block)
            } else if (det.title === "Heading 3") {
                insertOrUpdateBlock(editor, {
                    type: "heading",
                    props: {
                        level: 3,
                    },
                });
            } else if (det.title === "Numbered List") {
                insertOrUpdateBlock(editor, {
                    type: "numberedListItem",
                });
            }
            else if (det.title === "Bullet List") {
                insertOrUpdateBlock(editor, {
                    type: "bulletListItem",
                });
            }
            else if (det.title === "Paragraph") {
                insertOrUpdateBlock(editor, {
                    type: "paragraph",
                });
            }
            else if (det.title === "Image") {
                insertOrUpdateBlock(editor, {
                    type: "image",
                });
            }
            else if (det.title === "To-do List") {
                console.log('Editor todo', editor)
                insertOrUpdateBlock(editor,  {
                    type: 'checkbox',
                    // text: 'checked'
                    // content: {
                    //     text: 'Checked '
                    // }
                })
            }
            else if (det.title === "Alert") {
                insertOrUpdateBlock(editor,  {
                    type: 'alert'
                })
            }
            else if (det.title === 'Table View') {
                // tableModel.onOpen();           
                insertOrUpdateBlock(editor, {
									type: "tableView",
									content: {
										type: "tableContent",
										rows: [],
									},
								});            
            }
            else if (det.title === "API Table(Advanced)") {
							const url = window.prompt("Enter source");
							if (url) {
								const response = await fetch(url);

								if (response.ok) {
									const json = await response.json();
									console.log("Json fetch data", json);

									const block = insertOrUpdateBlock(editor, {
										type: "agTable",
										props: {
											type: "api",
											url,
										},
									});

                                    console.log('VLOCKKK', block);
								}
							} else {
							}
						} else if (det.title === "Table") {
							handleBlankClick();
						} else if (det.title === "Excel Table") {
							console.log("Excel Click");
							handleExcelClick();
						} else if (det.title === "Excel Table(Advanced)") {
							console.log("Excel Click");
							handleExcelAGClick();
						} else if (det.title === "JSON Table") {
							handleJSONClick();
						} else if (det.title === "JSON Table(Advanced)") {
							handleJSONAGClick();
						} else if (det.title === "API Table") {
							handleLinkClick();
						} else if (det.title === 'Goal Table') {
                            // const url = window.prompt("Enter source");
                            const url = "http://localhost/DarwinDocs/Tasks/getTasks";
							if (url) {
                                insertOrUpdateBlock(editor, {
                                                                type: "agTable",
                                                                props: {
                                                                    type: 'goal',
                                                                    url
                                                                },
                                                            });
                                                        }

                        }
                        else {
							console.log(det);
						}
}